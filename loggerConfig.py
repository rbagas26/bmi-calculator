import datetime
import logging

class Logger:
    logger = None

    def my_logger(self, log_file_name):
        if self.logger is None:
            self.logger = logging.getLogger(log_file_name)
            self.logger.setLevel(logging.DEBUG)
            now = datetime.datetime.now()
            handler = logging.FileHandler(f"logs/{log_file_name}{now.strftime('%Y-%m-%d')}.log")
            formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
            handler.setFormatter(formatter)
            self.logger.addHandler(handler)
        return self.logger