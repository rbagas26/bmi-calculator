# BMI Calculator
This application is running as a docker containerized python apps.

Use postman to use this application with the details below :

- API endpoints : http://54.158.67.207:8070/postjson
- Request body template :
{
    "height": 167,
    "weight": 64
}
- Method : POST

The response will be returned as json :
{
    "BMI": 22.95,
    "label": "normal"
}


If there is any incorrect steps when using the application such as incorrect body request, the return will be
{
    "ERROR": "Sorry, no numbers below zero"
}
