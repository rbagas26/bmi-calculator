FROM python:3.7

COPY . /app
WORKDIR /app

ENV VIRTUAL_ENV=/opt/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

RUN pip install --upgrade pip
RUN pip install flask
EXPOSE 8070

ENTRYPOINT ["python"]
CMD ["app.py"]
