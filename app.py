from flask import Flask
from flask import request
import logging
import loggerConfig

log = loggerConfig.Logger().my_logger("namaLog")
  
app = Flask(__name__)

@app.route('/postjson', methods = ['POST'])
def postJsonHandler():
    print (request.is_json)
    content = request.get_json()
    log.info(f"Request() request_body: {content}")

    result = dict()
    try:
        BMI = content['weight']/((content['height']/100)**2)
        formatedBMI = "{:.2f}".format(BMI)
        if content['weight'] > 0 and content['height'] > 0:
            if BMI >= 18.5 and BMI <= 25:
                label="normal"
                result['BMI'] = float(formatedBMI)
                result['label'] = label
            elif BMI < 18.5:
                label="underweight"
                result['BMI'] = float(formatedBMI)
                result['label'] = label
            elif BMI <= 0:
                label="Value not valid"
                result['BMI'] = float(formatedBMI)
                result['label'] = label
            else:
                label="overweight"
                result['BMI'] = float(formatedBMI)
                result['label'] = label
        else:
            result['ERROR'] = "Sorry, no numbers below zero"

    except:
        if "weight" and "height" in result:
            if type(content['weight']) == str or type(content['height']) == str:
                result['ERROR'] = "Input must be float or integer"
            elif content['weight'] <= 0 or content['height'] <= 0:
                result['ERROR'] = "Sorry, no numbers below zero"
        else:
            result['ERROR'] = "Unsupported input type"
    log.debug(f"Response: {result}")
    return result

if __name__ == '__main__':
    app.run(host='0.0.0.0', port= 8070, debug=False)

# if __name__ == '__main__':
#     app.run(host='0.0.0.0', port= 8070, debug=False)
